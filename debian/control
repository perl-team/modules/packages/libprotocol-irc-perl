Source: libprotocol-irc-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libfuture-perl <!nocheck>,
                     libtest-fatal-perl <!nocheck>
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libprotocol-irc-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libprotocol-irc-perl.git
Homepage: https://metacpan.org/release/Protocol-IRC
Rules-Requires-Root: no

Package: libprotocol-irc-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Multi-Arch: foreign
Description: IRC protocol handling
 This mix-in class provides a base layer of IRC message handling logic. It
 allows reading of IRC messages from a string buffer and dispatching them to
 handler methods on its instance.
 .
 Protocol::IRC::Client provides an extension to this logic that may be more
 convenient for IRC client implementations. Much of the code provided here is
 still useful in client applications, so the reader should be familiar with
 both modules.
